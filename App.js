import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import Home from './src/screens/Home';
//import Login from './src/screens/Login';


const Stack = createStackNavigator();

export default App = () => {

  const HomeStack = createStackNavigator();
  const HomeStackScreen = () =>
    (
      <HomeStack.Navigator
        screenOptions={{
          headerShown: false
        }}
      >
        <HomeStack.Screen
          name="Home"
          component={Home}>
        </HomeStack.Screen>

        {/* <HomeStack.Screen
          name="Login"
          component={Login}>
        </HomeStack.Screen> */}


      </HomeStack.Navigator>
    );


  return (
    <NavigationContainer>
      <HomeStackScreen />
    </NavigationContainer>
  );
};

//export default MainStackNavigator