import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Login from '../screens/Login';

class Home extends React.Component {
    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={styles.header}>
                 <Image style={styles.HeaderImage} source={require('../../assets/images/logo.png')} />
                 <View style={styles.txtContainer}>
                 <Text style={styles.play}>PLAY.</Text>
                 <Text style={styles.race}>RACE.</Text>
                 <Text style={styles.win}>WIN.</Text>
                 </View>
                </View>

                <View style={styles.footer}>
                <TouchableOpacity
                        style={styles.joinbutton}>
                        <Text style={styles.btntext}>JOIN</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        //onPress={() => this.props.navigation.navigate('Login')}
                        style={styles.loginbutton}>
                        <Text style={styles.btntext}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = {
    mainContainer: {
        flex: 1,
        backgroundColor: '#091430'
    },
    txtContainer:{
        flexDirection:'row',
        marginTop:20,
    },
    header:{
       height:'89%',
       alignItems: 'center',
       justifyContent: 'center',
    },
    HeaderImage:{
       //width:800,
       
    },
    footer:{
        flexDirection:'row',
        width:'100%',
        justifyContent:'center',
        alignItems:'flex-end',
    },
    play:
    {
        marginRight:20,
        color:'#8c8a8a',
        fontSize:22,  
        fontWeight : '900',    
    },
    race:
    {
        marginRight:20,
        color:'#d4d0d0',
        fontSize:22,
        fontWeight : '600',
    },
    win:
    {
        marginRight:20,
        color:'#fff',
        fontSize:22,
        fontWeight : 'bold',
    },
    joinbutton:{
        margin: 10,
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: '#fff',
        alignItems: "center",
        justifyContent:'center',
        backgroundColor: "#001a57",
        width:'30%',

    },
    loginbutton:{
        margin: 10,
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#FF0000',
        alignItems: "center",
        justifyContent:'center',
        backgroundColor: "#080f1f",
        width:'30%',
    },
    btntext:{
        color:'#fff',
    },
    btntext: {
        color: '#fff',
        fontSize: 15,
    },
}

export default Home;